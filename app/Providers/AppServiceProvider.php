<?php

namespace App\Providers;

use Jenssegers\Date\Date;
use Filament\Facades\Filament;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Filament::registerPages([
            \RyanChandler\FilamentProfile\Pages\Profile::class
        ]);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Filament::registerNavigationGroups([
            'Berita & Kegiatan PSKP',
            'Settings',
        ]);

        Date::setLocale('id');
        Paginator::defaultView('vendor.pagination.bootstrap-4');
    }
}
