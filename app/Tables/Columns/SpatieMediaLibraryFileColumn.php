<?php

namespace App\Tables\Columns;

use Filament\Tables\Columns\Column;

class SpatieMediaLibraryFileColumn extends Column
{
    protected string $view = 'tables.columns.spatie-media-library-file-column';

}
