<?php

namespace App\Filament\Resources\CallForPaperResource\Pages;

use Filament\Resources\Pages\ViewRecord;
use App\Filament\Resources\CallForPaperResource;

class ViewCallForPaper extends ViewRecord
{
    protected static string $resource = CallForPaperResource::class;

    protected static string $view = 'filament.resources.view-call-for-paper';

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
            ])
            ->pushActions([
                Tables\Actions\LinkAction::make('delete')
                    ->action(fn (CallForPaper $record) => $record->delete())
                    ->requiresConfirmation()
                    ->color('danger')
            ])
            ->filters([
                //
            ]);
    }
}
