<?php

namespace App\Filament\Resources\CallForPaperResource\Pages;

use App\Filament\Resources\CallForPaperResource;
use Filament\Resources\Pages\EditRecord;

class EditCallForPaper extends EditRecord
{
    protected static string $resource = CallForPaperResource::class;
}
