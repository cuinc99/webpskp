<?php

namespace App\Filament\Resources\CallForPaperResource\Pages;

use App\Filament\Resources\CallForPaperResource;
use Filament\Resources\Pages\CreateRecord;

class CreateCallForPaper extends CreateRecord
{
    protected static string $resource = CallForPaperResource::class;
}
