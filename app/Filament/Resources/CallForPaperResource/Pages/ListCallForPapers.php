<?php

namespace App\Filament\Resources\CallForPaperResource\Pages;

use App\Filament\Resources\CallForPaperResource;
use Filament\Resources\Pages\ListRecords;

class ListCallForPapers extends ListRecords
{
    protected static string $resource = CallForPaperResource::class;
}
