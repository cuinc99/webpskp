<?php

namespace App\Filament\Resources;

use Closure;
use Filament\Forms;
use Filament\Tables;
use App\Models\Gallery;
use Filament\Resources\Form;
use Filament\Resources\Table;
use Filament\Resources\Resource;
use App\Filament\Resources\GalleryResource\Pages;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;
use App\Filament\Resources\GalleryResource\RelationManagers;
use Filament\Forms\Components\SpatieMediaLibraryMultipleFileUpload;

class GalleryResource extends Resource
{
    protected static ?string $model = Gallery::class;

    protected static ?string $navigationIcon = 'heroicon-o-photograph';

    protected static ?string $recordTitleAttribute = 'title';

    protected static ?int $navigationSort = 4;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('title')
                    ->required()
                    ->maxLength(255)
                    ->columnSpan(12),
                Forms\Components\Radio::make('type')
                    ->options(Gallery::TYPE_RADIO)
                    ->required()
                    ->reactive()
                    ->columnSpan(12),
                SpatieMediaLibraryMultipleFileUpload::make('photo')
                    ->collection('gallery_photos')
                    ->helperText('Tambahkan photo hanya apabila anda memilih tipe photo.')
                    ->hidden(fn (Closure $get) => $get('type') !== 'Photo')
                    ->columnSpan(12),
                Forms\Components\TextInput::make('link')
                    ->label('Link Youtube')
                    ->url()
                    // ->helperText('Isi kolom link youtube hanya apabila anda memilih tipe video.')
                    ->maxLength(255)
                    ->hidden(fn (Closure $get) => $get('type') !== 'Video')
                    ->columnSpan(12),
                Forms\Components\Textarea::make('description')
                    ->columnSpan(12),
                SpatieMediaLibraryFileUpload::make('cover')
                    ->columnSpan(12),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\SpatieMediaLibraryImageColumn::make('cover')
                    ->conversion('thumb'),
                Tables\Columns\TextColumn::make('title')
                    ->searchable(),
                Tables\Columns\BadgeColumn::make('type')
                    ->colors([
                        'primary',
                        'success' => 'Photo',
                        'warning' => 'Video',
                    ]),
            ])
            ->pushActions([
                Tables\Actions\LinkAction::make('delete')
                    ->action(fn (Gallery $record) => $record->delete())
                    ->requiresConfirmation()
                    ->color('danger')
            ])
            ->filters([
                Tables\Filters\SelectFilter::make('type')
                    ->options(Gallery::TYPE_RADIO),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListGalleries::route('/'),
            'create' => Pages\CreateGallery::route('/create'),
            'view' => Pages\ViewGallery::route('/{record}'),
            'edit' => Pages\EditGallery::route('/{record}/edit'),
        ];
    }
}
