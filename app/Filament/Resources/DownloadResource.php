<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use App\Models\Download;
use Filament\Resources\Form;
use Filament\Resources\Table;
use Filament\Resources\Resource;
use App\Filament\Resources\DownloadResource\Pages;
use App\Tables\Columns\SpatieMediaLibraryFileColumn;
use App\Filament\Resources\DownloadResource\RelationManagers;

class DownloadResource extends Resource
{
    protected static ?string $model = Download::class;

    protected static ?string $navigationIcon = 'heroicon-o-document-download';

    protected static ?string $recordTitleAttribute = 'title';

    protected static ?int $navigationSort = 5;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Radio::make('category')
                    ->options(Download::CATEGORY_SELECT)
                    ->required()
                    ->columnSpan(12),
                Forms\Components\TextInput::make('title')
                    ->required()
                    ->maxLength(255)
                    ->columnSpan(12),
                Forms\Components\Textarea::make('description')
                    ->columnSpan(12),
                Forms\Components\SpatieMediaLibraryMultipleFileUpload::make('files')
                    ->collection('files')
                    ->required()
                    ->columnSpan(12)
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
        ->columns([
            Tables\Columns\TextColumn::make('title')
                ->searchable(),
            Tables\Columns\BadgeColumn::make('category')
                ->colors([
                    'primary',
                    'success' => 'Panduan',
                    'warning' => 'Ecertificate',
                    'info' => 'Form',
                ]),
            Tables\Columns\TextColumn::make('description')
                ->limit(50),
            SpatieMediaLibraryFileColumn::make('files'),
            // Tables\Columns\TextColumn::make('files'),
            // Tables\Columns\TextColumn::make('download_count'),
        ])
        ->pushActions([
            Tables\Actions\LinkAction::make('delete')
                ->action(fn (Download $record) => $record->delete())
                ->requiresConfirmation()
                ->color('danger')
        ])
        ->filters([
            Tables\Filters\SelectFilter::make('category')
                ->options(Download::CATEGORY_SELECT),
        ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDownloads::route('/'),
            'create' => Pages\CreateDownload::route('/create'),
            'edit' => Pages\EditDownload::route('/{record}/edit'),
        ];
    }
}
