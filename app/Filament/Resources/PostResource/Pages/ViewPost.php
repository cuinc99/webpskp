<?php

namespace App\Filament\Resources\PostResource\Pages;

use App\Models\Post;
use Filament\Resources\Pages\Page;
use App\Filament\Resources\PostResource;
use Filament\Resources\Pages\ViewRecord;

class ViewPost extends ViewRecord
{
    protected static string $resource = PostResource::class;
}
