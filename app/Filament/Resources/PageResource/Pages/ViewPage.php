<?php

namespace App\Filament\Resources\PageResource\Pages;

use Filament\Resources\Pages\Page;
use App\Filament\Resources\PageResource;
use Filament\Resources\Pages\ViewRecord;

class ViewPage extends ViewRecord
{
    protected static string $resource = PageResource::class;
}
