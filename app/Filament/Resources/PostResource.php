<?php

namespace App\Filament\Resources;

use Filament\Forms;
use App\Models\Post;
use Filament\Tables;
use Filament\Resources\Form;
use Filament\Resources\Table;
use Filament\Resources\Resource;
use Illuminate\Database\Eloquent\Model;
use App\Filament\Resources\PostResource\Pages;
use App\Filament\Resources\PostResource\RelationManagers;
use Mohamedsabil83\FilamentFormsTinyeditor\Components\TinyEditor;

class PostResource extends Resource
{
    protected static ?string $model = Post::class;

    protected static ?string $navigationIcon = 'heroicon-o-book-open';

    protected static ?string $recordTitleAttribute = 'title';

    protected static ?string $navigationGroup = 'Berita & Kegiatan PSKP';

    protected static ?int $navigationSort = 1;

    public static function getGlobalSearchResultUrl(Model $record): string
    {
        return url('/admin/posts/' . $record->id);
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\BelongsToSelect::make('category_id')
                    ->relationship('category', 'name')
                    ->required()
                    ->columnSpan(12),
                Forms\Components\TextInput::make('title')
                    ->required()
                    ->maxLength(255)
                    ->columnSpan(12),
                TinyEditor::make('content')
                    ->fileAttachmentsDisk('public')
                    ->fileAttachmentsDirectory('post_attach')
                    ->columnSpan(12),
                Forms\Components\DateTimePicker::make('published_at')
                    ->columnSpan(12),
                Forms\Components\Toggle::make('is_slider')
                    ->label('Show this post to slider?')
                    ->helperText('Tampilkan postingan ini di bagian slider.')
                    ->columnSpan(12),
                Forms\Components\SpatieMediaLibraryFileUpload::make('cover')
                    ->image()
                    ->required()
                    ->columnSpan(12),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\SpatieMediaLibraryImageColumn::make('cover')
                    ->conversion('thumb'),
                Tables\Columns\TextColumn::make('title')
                    ->limit('50')
                    ->searchable(),
                Tables\Columns\TextColumn::make('category.name'),
                Tables\Columns\TextColumn::make('published_at')
                    ->dateTime()
                    ->sortable(),
                Tables\Columns\BooleanColumn::make('is_slider'),
            ])
            ->pushActions([
                Tables\Actions\LinkAction::make('delete')
                    ->action(fn (Post $record) => $record->delete())
                    ->requiresConfirmation()
                    ->color('danger')
            ])
            ->defaultSort('published_at', 'desc')
            ->filters([
                Tables\Filters\SelectFilter::make('category')
                    ->relationship('category', 'name'),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPosts::route('/'),
            'create' => Pages\CreatePost::route('/create'),
            'view' => Pages\ViewPost::route('/{record}'),
            'edit' => Pages\EditPost::route('/{record}/edit'),
        ];
    }
}
