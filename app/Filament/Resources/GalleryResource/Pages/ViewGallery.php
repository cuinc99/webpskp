<?php

namespace App\Filament\Resources\GalleryResource\Pages;

use Filament\Resources\Pages\ViewRecord;
use App\Filament\Resources\GalleryResource;

class ViewGallery extends ViewRecord
{
    protected static string $resource = GalleryResource::class;
}
