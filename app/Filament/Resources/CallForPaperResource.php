<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use App\Models\CallForPaper;
use Filament\Resources\Form;
use Filament\Resources\Table;
use Filament\Resources\Resource;
use App\Filament\Resources\CallForPaperResource\Pages;
use App\Filament\Resources\CallForPaperResource\RelationManagers;
use Mohamedsabil83\FilamentFormsTinyeditor\Components\TinyEditor;

class CallForPaperResource extends Resource
{
    protected static ?string $model = CallForPaper::class;

    protected static ?string $navigationIcon = 'heroicon-o-book-open';

    protected static ?string $recordTitleAttribute = 'title';

    protected static ?string $navigationGroup = 'Berita & Kegiatan PSKP';

    protected static ?int $navigationSort = 2;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('title')
                    ->required()
                    ->maxLength(255)
                    ->columnSpan(12),
                TinyEditor::make('description')
                    ->fileAttachmentsDisk('public')
                    ->fileAttachmentsDirectory('cfp_attach')
                    ->columnSpan(12),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title'),
                Tables\Columns\TextColumn::make('registrations_count')
                    ->label('Number of Registration')
                    ->counts('registrations')
            ])
            ->pushActions([
                Tables\Actions\LinkAction::make('delete')
                    ->action(fn (CallForPaper $record) => $record->delete())
                    ->requiresConfirmation()
                    ->color('danger')
            ])
            ->filters([
                //
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCallForPapers::route('/'),
            'create' => Pages\CreateCallForPaper::route('/create'),
            'edit' => Pages\EditCallForPaper::route('/{record}/edit'),
            'view' => Pages\ViewCallForPaper::route('/{record}/registration'),
        ];
    }
}
