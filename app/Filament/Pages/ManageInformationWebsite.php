<?php

namespace App\Filament\Pages;

use Filament\Pages\SettingsPage;
use Filament\Forms\Components\TextInput;
use App\Settings\InformationWebsiteSettings;

class ManageInformationWebsite extends SettingsPage
{
    protected static ?string $navigationIcon = 'heroicon-o-cog';

    protected static string $settings = InformationWebsiteSettings::class;

    protected static ?string $navigationGroup = 'Settings';

    protected static ?string $navigationLabel = 'Information Website';

    protected function getFormSchema(): array
    {
        return [
            TextInput::make('email')
                ->required()
                ->columnSpan(12),
            TextInput::make('contact_name')
                ->required()
                ->columnSpan(12),
            TextInput::make('contact_number')
                ->required()
                ->columnSpan(12),
            TextInput::make('address')
                ->required()
                ->columnSpan(12),
            TextInput::make('facebook')
                ->required()
                ->columnSpan(12),
            TextInput::make('twitter')
                ->required()
                ->columnSpan(12),
            TextInput::make('instagram')
                ->required()
                ->columnSpan(12),
            TextInput::make('youtube')
                ->required()
                ->columnSpan(12),
        ];
    }
}
