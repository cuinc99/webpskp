<?php

namespace App\Http\Controllers;

use App\Models\CallForPaper;
use App\Models\Page;
use App\Models\Post;
use App\Models\Event;
use App\Models\Gallery;
use App\Models\Category;
use App\Models\Download;
use App\Models\Registration;
use Illuminate\Http\Request;
use App\Settings\InformationMemberSettings;

class FrontendController extends Controller
{
    public function index()
    {
        $posts = Post::query()
            ->with('visits')
            ->whereNotNUll('published_at')
            ->where('category_id', 1)
            ->where('is_slider', false)
            ->orderBy('published_at', 'desc')
            ->limit(3)
            ->get();

        $sliders = Post::query()
            ->with('visits')
            ->whereNotNUll('published_at')
            ->where('is_slider', true)
            ->orderBy('published_at', 'desc')
            ->limit(3)
            ->get();

        $callForPaper = CallForPaper::latest()->first();

        return view('frontend.index', compact('posts', 'sliders', 'callForPaper'));
    }

    public function postIndex($category)
    {
        $cat = str_replace("-", " ", $category);
        $category = Category::where('name', $cat)->firstOrFail();

        $posts = Post::query()
            ->with('visits')
            ->whereNotNUll('published_at')
            ->where('category_id', $category->id)
            ->orderBy('published_at', 'desc')
            ->paginate(9);

        return view('frontend.posts.index', compact('posts', 'cat'));
    }

    public function postDetail(Post $post)
    {
        $newPosts = Post::query()
            ->with('visits')
            ->whereNotNUll('published_at')
            ->orderBy('published_at', 'desc')
            ->limit(4)
            ->get();

        $popularPosts = visits('App\Models\Post')->top(4);

        $post->vzt()->increment();

        return view('frontend.posts.detail', compact('newPosts', 'popularPosts', 'post'));
    }

    public function pageDetail(Page $page)
    {
        return view('frontend.pages.detail', compact('page'));
    }

    public function downloadIndex($category)
    {
        $cat = str_replace("-", " ", $category);
        $downloads = Download::where('category', $cat)->latest()->get();

        return view('frontend.downloads.index', compact('downloads', 'cat'));
    }

    public function galleryIndex()
    {
        $galleries = Gallery::latest()->paginate(9);

        return view('frontend.galleries.index', compact('galleries'));
    }

    public function galleryDetail(Gallery $gallery)
    {
        return view('frontend.galleries.detail', compact('gallery'));
    }

    public function eventIndex()
    {
        $events = Event::latest()->paginate(8);

        return view('frontend.events.index', compact('events'));
    }

    public function eventDetail(Event $event)
    {
        return view('frontend.events.detail', compact('event'));
    }

    public function callForPaper()
    {
        $callForPaper = CallForPaper::latest()->first();

        return view('frontend.callForPapers.index', compact('callForPaper'));
    }

    public function callForPaperPost(Request $request)
    {
        Registration::create($request->all());
        session()->flash('message', 'Berhasil terkirim');
        return back();
    }
}
