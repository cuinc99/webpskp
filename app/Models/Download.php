<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Download extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $guarded = [];

    const CATEGORY_SELECT = [
        'Panduan' => 'Panduan',
        'Ecertificate' => 'Ecertificate',
        'Form' => 'Form',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 120, 120);
    }

    public function getFilesAttribute()
    {
        return $this->getMedia('files');
    }

    public function getContentsAttribute()
    {
        return $this->getMedia();
    }
}
