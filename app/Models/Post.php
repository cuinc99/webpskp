<?php

namespace App\Models;

use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Post extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $guarded = [];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 370, 255);
        $this->addMediaConversion('detail')->fit('crop', 750, 500);
        $this->addMediaConversion('slider')->fit('crop', 1970, 798);

    }

    public function getCoverAttribute()
    {
        $file = $this->getMedia('cover')->last();
        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->preview   = $file->getUrl('preview');
            $file->detail   = $file->getUrl('detail');
            $file->detail   = $file->getUrl('slider');
        }

        return $file;
    }

    public function getCoverImageAttribute()
    {
        $file = $this->getMedia()->last();
        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->preview   = $file->getUrl('preview');
            $file->detail   = $file->getUrl('detail');
            $file->detail   = $file->getUrl('slider');
        }

        return $file;
    }

    public function getSlugAttribute()
    {
        return Str::slug($this->title);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function vzt()
    {
        return visits($this);
    }

    public function visits()
    {
        return visits($this)->relation();
    }
}
