<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Gallery extends Model implements HasMedia
{
    use InteractsWithMedia;
    use HasFactory;

    public const TYPE_RADIO = [
        'Photo' => 'Photo',
        'Video' => 'Video',
    ];

    public $table = 'galleries';

    protected $appends = [
        'photo',
        'cover',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'description',
        'type',
        'link',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function getSlugAttribute()
    {
        $title = Str::limit($this->title, 50, '');
        return Str::slug($title);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 120, 120);
        $this->addMediaConversion('detail')->fit('crop', 360, 224);
    }

    public function getPhotoAttribute()
    {
        return $this->getMedia('gallery_photos');
    }

    public function getCoverAttribute()
    {
        $file = $this->getMedia('cover')->last();
        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->preview   = $file->getUrl('preview');
            $file->detail   = $file->getUrl('detail');
        }

        return $file;
    }

    public function getCoverImageAttribute()
    {
        $file = $this->getMedia()->last();
        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->preview   = $file->getUrl('preview');
            $file->detail   = $file->getUrl('detail');
        }

        return $file;
    }
}
