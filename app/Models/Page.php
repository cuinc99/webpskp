<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    const CATEGORY_SELECT = [
        'Tentang Kami' => 'Tentang Kami',
        'Hibah - Internal' => 'Hibah - Internal',
        'Hibah - Nasional' => 'Hibah - Nasional',
        'Hibah - Internasional' => 'Hibah - Internasional',
        'Kerjasama - Lokal' => 'Kerjasama - Lokal',
        'Kerjasama - Nasional' => 'Kerjasama - Nasional',
        'Kerjasama - Internasional' => 'Kerjasama - Internasional',
        'Karya Ilmiah Remaja' => 'Karya Ilmiah Remaja',
    ];

    protected $guarded = [];
}
