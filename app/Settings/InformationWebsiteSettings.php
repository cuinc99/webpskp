<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class InformationWebsiteSettings extends Settings
{
    public string $email;
    public string $contact_name;
    public string $contact_number;
    public string $address;
    public string $facebook;
    public string $twitter;
    public string $instagram;
    public string $youtube;

    public static function group(): string
    {
        return 'infomation_website';
    }
}
