<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            [
                'id' => 1,
                'title' => 'Sejarah PSKP',
                'category' => 'Tentang Kami',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 2,
                'title' => 'Visi & Misi FK Unizar',
                'category' => 'Tentang Kami',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 3,
                'title' => 'Sekapur Sirih',
                'category' => 'Tentang Kami',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 4,
                'title' => 'Struktur Organisasi',
                'category' => 'Tentang Kami',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 5,
                'title' => 'xxx',
                'category' => 'Tentang Kami',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 6,
                'title' => 'Alur Pengajuan',
                'category' => 'Hibah - Internal',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 7,
                'title' => 'Timeline',
                'category' => 'Hibah - Internal',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 8,
                'title' => 'Aturan',
                'category' => 'Hibah - Internal',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 9,
                'title' => 'FAQ',
                'category' => 'Hibah - Internal',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 10,
                'title' => 'Nasional',
                'category' => 'Hibah - Nasional',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 11,
                'title' => 'Internasional',
                'category' => 'Hibah - Internasional',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 12,
                'title' => 'Klinik',
                'category' => 'Kerjasama - Lokal',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 13,
                'title' => 'Dinkes',
                'category' => 'Kerjasama - Lokal',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 14,
                'title' => 'Rumah Sakit',
                'category' => 'Kerjasama - Lokal',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 15,
                'title' => 'Dispar NTB',
                'category' => 'Kerjasama - Lokal',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 16,
                'title' => 'Kemenparekraf',
                'category' => 'Kerjasama - Nasional',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 17,
                'title' => 'Internasional',
                'category' => 'Kerjasama - Internasional',
                'content' => 'lorem ipsum...',
            ],
            [
                'id' => 18,
                'title' => 'Informasi Karya Ilmiah Remaja',
                'category' => 'Karya Ilmiah Remaja',
                'content' => 'lorem ipsum...',
            ],
        ];

        Page::insert($pages);
    }
}
