<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;
use Database\Seeders\PageSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password'),
        ]);

        $this->call([
            PageSeeder::class,
        ]);

        Category::create(['name' => 'Berita']);
        Category::create(['name' => 'Penelitian']);
        Category::create(['name' => 'Pengabdian Kepada Masyarakat']);
        Category::create(['name' => 'Seminar']);
        Category::create(['name' => 'Workshop']);
        Category::create(['name' => 'Pelatihan']);

        \App\Models\Post::factory(20)->create();
        \App\Models\Download::factory(10)->create();
        \App\Models\Gallery::factory(10)->create();
        \App\Models\Event::factory(10)->create();
    }
}
