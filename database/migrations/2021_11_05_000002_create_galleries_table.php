<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalleriesTable extends Migration
{
    public function up()
    {
        Schema::create('galleries', function (Blueprint $table) {
            $table->id('id');
            $table->string('title');
            $table->longText('description')->nullable();
            $table->string('type');
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }
}
