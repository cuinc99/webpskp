<?php

namespace Database\Factories;

use App\Models\Gallery;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class GalleryFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Gallery::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        $a = ['Photo', 'Video'];
        return [
            'title' => $this->faker->sentence($nbWords = 10, $variableNbWords = true),
            'description' => $this->faker->paragraph,
            'type' => $a[array_rand($a)],
            'link' => $this->faker->url,
        ];
    }
}
