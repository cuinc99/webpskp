<?php

namespace Database\Factories;

use App\Models\Event;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Event::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        return [
            'name' => $this->faker->sentence($nbWords = 10, $variableNbWords = true),
            'start_at' => $this->faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
            'end_at' => $this->faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
            'description' => $this->faker->paragraph,
        ];
    }
}
