<?php

namespace Database\Factories;

use App\Models\Download;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class DownloadFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Download::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        $cat = ['Panduan', 'E-certificate', 'Form'];

        return [
            'title' => $this->faker->sentence($nbWords = 10, $variableNbWords = true),
            'category' => $cat[array_rand($cat)],
            'description' => $this->faker->paragraph,
            'download_count' => rand(0, 100),
        ];
    }
}
