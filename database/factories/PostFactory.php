<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Post::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        return [
            'title' => $this->faker->sentence($nbWords = 10, $variableNbWords = true),
            'content' => $this->faker->paragraph,
            'published_at' => $this->faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
            'category_id' => rand(1, 6),
            'user_id' => 1,
            'is_slider' => rand(1, 0),
        ];
    }
}
