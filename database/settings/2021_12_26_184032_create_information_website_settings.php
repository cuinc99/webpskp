<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateInformationWebsiteSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('infomation_website.email', 'pskp@fkunizar.ac.id');
        $this->migrator->add('infomation_website.contact_name', 'Nanang Alfian');
        $this->migrator->add('infomation_website.contact_number', '703 (123) 4567');
        $this->migrator->add('infomation_website.address', 'Jl. Unizar No.20, Turida, Kec. Sandubaya, Kota Mataram, Nusa Tenggara Barat. 83232');
        $this->migrator->add('infomation_website.facebook', 'https://facebook.com/xxx');
        $this->migrator->add('infomation_website.twitter', 'https://twitter.com/xxx');
        $this->migrator->add('infomation_website.instagram', 'https://instagram.com/xxx');
        $this->migrator->add('infomation_website.youtube', 'https://youtube.com/c/xxx');
    }
}
