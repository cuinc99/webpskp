<x-filament::page>
    {{-- {{ $this->record->title }} --}}

    <div class="filament-tables-component">
        <div class="border border-gray-300 shadow-sm bg-white rounded-xl filament-tables-container">
            <div class="flex items-center justify-between p-2 h-2"></div>
            <div class="overflow-y-auto relative border-t">
                <table class="w-full text-left rtl:text-right divide-y table-auto filament-tables-table">
                    <thead>
                        <tr class="bg-gray-50">

                            <th class="p-0 filament-tables-header-cell">
                                <button type="button"
                                    class="flex items-center w-full px-4 py-2 whitespace-nowrap space-x-1 rtl:space-x-reverse font-medium text-sm text-gray-600 cursor-default ">
                                    <span>
                                        Nama
                                    </span>

                                </button>
                            </th>
                            <th class="p-0 filament-tables-header-cell">
                                <button type="button"
                                    class="flex items-center w-full px-4 py-2 whitespace-nowrap space-x-1 rtl:space-x-reverse font-medium text-sm text-gray-600 cursor-default ">
                                    <span>
                                        Email
                                    </span>

                                </button>
                            </th>
                            <th class="p-0 filament-tables-header-cell">
                                <button type="button"
                                    class="flex items-center w-full px-4 py-2 whitespace-nowrap space-x-1 rtl:space-x-reverse font-medium text-sm text-gray-600 cursor-default ">
                                    <span>
                                        No. Hp
                                    </span>

                                </button>
                            </th>
                            <th class="p-0 filament-tables-header-cell">
                                <button type="button"
                                    class="flex items-center w-full px-4 py-2 whitespace-nowrap space-x-1 rtl:space-x-reverse font-medium text-sm text-gray-600 cursor-default ">
                                    <span>
                                        Jurusan/Program Studi
                                    </span>

                                </button>
                            </th>
                            <th class="p-0 filament-tables-header-cell">
                                <button type="button"
                                    class="flex items-center w-full px-4 py-2 whitespace-nowrap space-x-1 rtl:space-x-reverse font-medium text-sm text-gray-600 cursor-default ">
                                    <span>
                                        Status
                                    </span>

                                </button>
                            </th>
                            <th class="p-0 filament-tables-header-cell">
                                <button type="button"
                                    class="flex items-center w-full px-4 py-2 whitespace-nowrap space-x-1 rtl:space-x-reverse font-medium text-sm text-gray-600 cursor-default ">
                                    <span>
                                        Instansi
                                    </span>

                                </button>
                            </th>
                            <th class="p-0 filament-tables-header-cell">
                                <button type="button"
                                    class="flex items-center w-full px-4 py-2 whitespace-nowrap space-x-1 rtl:space-x-reverse font-medium text-sm text-gray-600 cursor-default ">
                                    <span>
                                        Jumlah Artikel
                                    </span>

                                </button>
                            </th>
                            <th class="p-0 filament-tables-header-cell">
                                <button type="button"
                                    class="flex items-center w-full px-4 py-2 whitespace-nowrap space-x-1 rtl:space-x-reverse font-medium text-sm text-gray-600 cursor-default ">
                                    <span>
                                        Judul Sementara
                                    </span>

                                </button>
                            </th>
                            <th class="p-0 filament-tables-header-cell">
                                <button type="button"
                                    class="flex items-center w-full px-4 py-2 whitespace-nowrap space-x-1 rtl:space-x-reverse font-medium text-sm text-gray-600 cursor-default ">
                                    <span>
                                        Tipe Presentasi
                                    </span>

                                </button>
                            </th>

                            {{-- <th class="w-5"></th> --}}
                        </tr>
                    </thead>

                    <tbody class="divide-y whitespace-nowrap">
                        @forelse ($this->record->registrations as $registration)
                            <tr class="filament-tables-row" wire:key="1">

                                <td class="filament-tables-cell">
                                    <div class="px-4 py-3 filament-tables-text-column">
                                        {{ $registration->name }}
                                    </div>
                                </td>

                                <td class="filament-tables-cell">
                                    <div class="px-4 py-3 filament-tables-text-column">
                                        {{ $registration->email }}
                                    </div>
                                </td>

                                <td class="filament-tables-cell">
                                    <div class="px-4 py-3 filament-tables-text-column">
                                        {{ $registration->phone_number }}
                                    </div>
                                </td>

                                <td class="filament-tables-cell">
                                    <div class="px-4 py-3 filament-tables-text-column">
                                        {{ $registration->major }}
                                    </div>
                                </td>

                                <td class="filament-tables-cell">
                                    <div class="px-4 py-3 filament-tables-text-column">
                                        {{ $registration->status }}
                                    </div>
                                </td>

                                <td class="filament-tables-cell">
                                    <div class="px-4 py-3 filament-tables-text-column">
                                        {{ $registration->agency }}
                                    </div>
                                </td>

                                <td class="filament-tables-cell">
                                    <div class="px-4 py-3 filament-tables-text-column">
                                        {{ $registration->number_of_article }}
                                    </div>
                                </td>

                                <td class="filament-tables-cell">
                                    <div class="px-4 py-3 filament-tables-text-column">
                                        {{ $registration->temporary_title }}
                                    </div>
                                </td>

                                <td class="filament-tables-cell">
                                    <div class="px-4 py-3 filament-tables-text-column">
                                        {{ $registration->type_presentation }}
                                    </div>
                                </td>

                                {{-- <td class="px-4 py-3 whitespace-nowrap filament-tables-actions-cell">
                                    <div class="flex items-center gap-4 justify-end">
                                        <a class="inline-flex items-center justify-center gap-0.5 font-medium hover:underline focus:outline-none focus:underline filament-link text-sm text-primary-600 hover:text-primary-500 filament-tables-link-action"
                                            href="#">
                                            <svg class="filament-button-icon w-3 h-3 mr-1 -ml-2 rtl:ml-1 rtl:-mr-2"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                                fill="currentColor" aria-hidden="true">
                                                <path d="M10 12a2 2 0 100-4 2 2 0 000 4z"></path>
                                                <path fill-rule="evenodd"
                                                    d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z"
                                                    clip-rule="evenodd"></path>
                                            </svg>
                                            View
                                        </a>
                                    </div>
                                </td> --}}
                            </tr>
                        @empty
                        <tr class="filament-tables-row">

                            <td class="filament-tables-cell" colspan="9">
                                <div class="px-4 py-3 filament-tables-text-column">
                                    Belum ada pendaftar
                                </div>
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-filament::page>
