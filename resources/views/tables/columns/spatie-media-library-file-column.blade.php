<div>
    @foreach($getState() as $key => $media)
        <a href="{{ $media->getUrl() }}" target="_blank" class="text-sm font-medium transition hover:underline focus:outline-none focus:underline text-success-600 hover:text-success-500">
            Show File {{ $key + 1 }}
        </a>
        <br>
    @endforeach
</div>
