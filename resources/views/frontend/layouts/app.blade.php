
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/img/default/favicon.png">
    <title>PSKP - @yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="/front/assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- This is for the animation CSS -->
    <link href="/front/assets/node_modules/aos/dist/aos.css" rel="stylesheet">
    <link href="/front/assets/node_modules/bootstrap-touch-slider/bootstrap-touch-slider.css" rel="stylesheet" media="all">
    <link href="/front/assets/node_modules/owl.carousel/dist/assets/owl.theme.green.css" rel="stylesheet">
    <!-- This css we made it from our predefine componenet
    we just copy that css and paste here you can also do that -->
    <link href="/front/css/demo.css" rel="stylesheet">
    <!-- Common style CSS -->
    <link href="/front/css/style.css" rel="stylesheet">
    <link href="/front/css/yourstyle.css" rel="stylesheet">
    <link href="/front/css/blog/blog-homepage.css" rel="stylesheet">
    <link href="/front/css/features/features21-30.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    @yield('styles')
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">PSKP FK Unizar</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Top header  -->
        <!-- ============================================================== -->
        <div class="topbar">
            <!-- ============================================================== -->
            <!-- Header 16  -->
            <!-- ============================================================== -->
            <div class="header16 po-relative">
                <!-- Topbar  -->
                <div class="h16-topbar">
                    <div class="container">
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand hidden-lg-up" href="#">Top Menu</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header16a" aria-controls="header16a" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="sl-icon-options"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="header16a">
                                <ul class="navbar-nav font-14">
                                    <li class="nav-item">Selamat Datang di Website <span class="text-dark">Pusat Studi Kesehatan Pariwisata</span></li>
                                </ul>
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item"><a class="nav-link" href="mailto:{{ app(\App\Settings\InformationWebsiteSettings::class)->email }}"><i class="fa fa-envelope"></i> {{ app(\App\Settings\InformationWebsiteSettings::class)->email }}</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{ app(\App\Settings\InformationWebsiteSettings::class)->twitter }}"><i class="fa fa-twitter"></i></a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{ app(\App\Settings\InformationWebsiteSettings::class)->facebook }}"><i class="fa fa-facebook-square"></i></a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- Infobar  -->
                <div class="h16-infobar">
                    <div class="container">
                        <nav class="navbar navbar-expand-lg h16-info-bar">
                            <a class="navbar-brand"><img src="/front/images/galeri/logo pskp.png" alt="logo pskp" style="height: 70px;"/></a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h16-info" aria-controls="h16-info" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="sl-icon-options-vertical"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="h16-info">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link">
                                            <div class="display-6 m-r-10"><i class="icon-Over-Time text-danger"></i></div>
                                            <div><small>Senin s.d Sabtu - <span class="text-dark">8:00-17:00</span> <br/>Minggu - Libur</small></div>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link">
                                            <div class="display-6 m-r-10"><i class="icon-Phone-2 text-danger"></i></div>
                                            <div><small>Kontak</small>
                                                <h5 class="font-bold">{{ app(\App\Settings\InformationWebsiteSettings::class)->contact_number }}</h5></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- Navbar  -->
                <div class="h16-navbar">
                    <div class="container">
                        <nav class="navbar navbar-expand-lg h16-nav">
                            <a class="hidden-lg-up">Navigation</a>
                            <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#header16" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="ti-menu"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="header16">
                                @include('frontend.layouts.nav')
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item search"><a class="nav-link" href="javascript:void(0)">Kontak</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Header 16  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- Top header  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" style="color: black;">

                @yield('content')

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Back to top -->
            <!-- ============================================================== -->
            <a class="bt-top btn btn-circle btn-lg btn-danger" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="/front/assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="/front/assets/node_modules/popper/dist/popper.min.js"></script>
    <script src="/front/assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
    <!-- This is for the animation -->
    <script src="/front/assets/node_modules/aos/dist/aos.js"></script>
    <!--Custom JavaScript -->
    <script src="/front/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="/front/assets/node_modules/bootstrap-touch-slider/bootstrap-touch-slider.js"></script>
    <script src="/front/assets/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="/front/assets/node_modules/jquery.touchSwipe.min.js"></script>
    <script src="/front/assets/node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="/front/js/isotope.pkgd.min.js"></script>
    <script src="/front/js/portfolio.js"></script>
    <script>
    $('.tgl-cl').on('click', function() {
        $('body .h17-main-nav').toggleClass("show");
    });
    // This is for header toggle
    $('.op-clo').on('click', function() {
            $('body .h7-nav-box').toggleClass("show");
        });
    </script>
    <script type="text/javascript">
    $('#slider10').bsTouchSlider();
    $(".carousel .carousel-inner").swipe({
        swipeLeft: function(event, direction, distance, duration, fingerCount) {
            this.parent().carousel('next');
        },
        swipeRight: function() {
            this.parent().carousel('prev');
        },
        threshold: 0
    });
    /*******************************/
    // this is for the testimonial 9
    /*******************************/
    $('.testi9').owlCarousel({
        loop: true,
        margin: 30,
        nav: false,
        dots: true,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            1650: {
                items: 1
            }
        }
    });
    // This is for the static slider 6
    $('.video-img').on('click', function() {
        $(this).addClass('hide');
        $('.embed-responsive').show()
            .removeClass('hide');
        $("video").each(function() { this.play() });
    });
    </script>
    @yield('scripts')
</body>

</html>
