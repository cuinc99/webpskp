<div class="hover-dropdown">
    <ul class="navbar-nav">
        <li class="nav-item"> <a class="nav-link" href="/">
      Beranda
    </a>
        </li>
        <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="h6-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Tentang Kami <i class="fa fa-angle-down m-l-5"></i>
    </a>
            <ul class="b-none dropdown-menu font-14 animated fadeInUp">
                <li><a class="dropdown-item" href="{{ url('page/1/sejarah-pskp.html') }}">PSKP</a></li>
                <li><a class="dropdown-item" href="{{ url('page/2/visi-misi.html') }}">Visi Misi FK Unizar</a></li>
                <li><a class="dropdown-item" href="{{ url('page/3/sekapur-sirih.html') }}">Sekapur Sirih</a></li>
                <li><a class="dropdown-item" href="{{ url('page/4/struktur-organisasi.html') }}">Struktur Organisasi</a></li>
                <li><a class="dropdown-item" href="{{ url('page/5/xxx.html') }}">xxx</a></li>
            </ul>
        </li>
        <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="h6-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Kegiatan PSKP <i class="fa fa-angle-down m-l-5"></i>
          </a>
                  <ul class="b-none dropdown-menu font-14 animated fadeInUp">
                      <li><a class="dropdown-item" href="{{ route('postIndex', 'penelitian') }}">Penelitian</a></li>
                      <li><a class="dropdown-item" href="{{ route('postIndex', 'pengabdian-kepada-masyarakat') }}">Pengabdian Kepada Masyarakat</a></li>
                      <li><a class="dropdown-item" href="{{ route('postIndex', 'seminar') }}">Seminar</a></li>
                      <li><a class="dropdown-item" href="{{ route('postIndex', 'workshop') }}">Workshop</a></li>
                      <li><a class="dropdown-item" href="{{ route('postIndex', 'pelatihan') }}">Pelatihan</a></li>
                  </ul>
              </li>
        <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="h6-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Hibah <i class="fa fa-angle-down m-l-5"></i>
        </a>
                <ul class="b-none dropdown-menu font-14 animated fadeInUp">
                    <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" href="javascript:void(0)" aria-haspopup="true" aria-expanded="false">Internal<i class="fa fa-angle-right ml-auto"></i></a>
                        <ul class="dropdown-menu menu-right font-14 b-none animated flipInY">
                            <li><a class="dropdown-item" href="{{ url('page/6/hibah-internal-alur-pengajuan.html') }}">Alur Pengajuan</a></li>
                            <li><a class="dropdown-item" href="{{ url('page/7/hibah-internal-timeline.html') }}">Timeline</a></li>
                            <li><a class="dropdown-item" href="{{ url('page/8/hibah-internal-aturan.html') }}">Aturan</a></li>
                            <li><a class="dropdown-item" href="{{ url('page/9/hibah-internal-faq.html') }}">FAQ</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item" href="{{ url('page/10/hibah-nasional.html') }}">Nasional</a></li>
                    <li><a class="dropdown-item" href="{{ url('page/11/hibah-internasional.html') }}">Internasional</a></li>
                </ul>
            </li>

        <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="h6-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Kerjasama <i class="fa fa-angle-down m-l-5"></i>
        </a>
                <ul class="b-none dropdown-menu font-14 animated fadeInUp">
                    <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" href="javascript:void(0)" aria-haspopup="true" aria-expanded="false">Lokal<i class="fa fa-angle-right ml-auto"></i></a>
                        <ul class="dropdown-menu menu-right font-14 b-none animated flipInY">
                            <li><a class="dropdown-item" href="{{ url('page/12/kerjasama-lokal-klinik.html') }}">Klinik</a></li>
                            <li><a class="dropdown-item" href="{{ url('page/13/kerjasama-lokal-dinkes.html') }}">Dinkes</a></li>
                            <li><a class="dropdown-item" href="{{ url('page/14/kerjasama-lokal-rumah-sakit.html') }}">Rumah Sakit</a></li>
                            <li><a class="dropdown-item" href="{{ url('page/15/kerjasama-lokal-dispar-ntb.html') }}">Dispar NTB</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu"> <a class="dropdown-toggle dropdown-item" href="javascript:void(0)" aria-haspopup="true" aria-expanded="false">Nasional<i class="fa fa-angle-right ml-auto"></i></a>
                        <ul class="dropdown-menu menu-right font-14 b-none animated flipInY">
                            <li><a class="dropdown-item" href="{{ url('page/16/kerjasama-nasional-kemenparekraf.html') }}">Kemenparekraf</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item" href="{{ url('page/17/kerjasama-internasional.html') }}">Internasional</a></li>
                </ul>
            </li>

            <li class="nav-item"> <a class="nav-link" href="{{ route('eventIndex') }}">
                Event
              </a>
                  </li>

    </ul>
</div>
