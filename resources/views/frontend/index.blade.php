@extends('frontend.layouts.app')

@section('title')
Pusat Studi Kesehatan Pariwisata | FK Unizar
@endsection

@section('styles')
<link href="/front/css/sliders/slider10.css" rel="stylesheet">
@endsection

@section('scripts')
@endsection

@section('content')
<video playsinline autoplay muted loop id="bgvid">
    <source src="/videos/video.mp4" type="video/mp4" />
</video>
<!-- ============================================================== -->
<!-- Slider 1  -->
<!-- ============================================================== -->
{{-- <section id="slider-sec" class="slider10">
    <div id="slider10" class="carousel bs-slider slide control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="7000">
        <ol class="carousel-indicators">

            @foreach ($sliders as $key => $slider)
            <li data-target="#slider7" data-slide-to="{{ $key }}" class="{{ $loop->first ? 'active' : '' }}"></li>
            @endforeach
        </ol>
        <!-- Wrapper For Slides -->
        <div class="carousel-inner" role="listbox">

            @foreach ($sliders as $slider)

            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                <!-- Slide Background -->
                @if($slider->cover_image)
                    <img src="{{ $slider->cover_image->getUrl('slider') }}" alt="cover" class="slide-image">
                @else
                    <img class="slide-image" src="/assets/images/slide2.jpg" alt="cover">
                @endif
                <!-- Slide Text Layer -->
                <div class="slide-text slide_style_left">
                    <div class="container" data-animation="animated fadeInLeft">
                        <div class="slide-content po-relative" style="background: rgba(0, 0, 0, 0.33); max-width: 500px; border-radius: 20px;">
                            <label class="text-dark label bg-warning"><b>{{ $slider->category->name ?? 'Tidak ada Kategori' }}</b> - {{ Date::parse($slider->published_at)->format('d F Y') }}</label>
                            <h2 data-animation="animated flipInX" class="m-b-10 m-t-0">
                                <a href="{{ route('postDetail', [$slider->id, $slider->slug]) }}" class="text-white link">{{ $slider->title }}</a>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>

            @endforeach
            <!-- End of Slide -->
            <!-- End of Wrapper For Slides -->
            <!-- Slider Control -->
            <div class="slider-control hide">
                <!-- Left Control -->
                <a class="left carousel-control-prev text-danger font-14" href="#slider7" role="button" data-slide="prev"> <span class="text-center bg-white ti-arrow-left" aria-hidden="true"></span> <b class="font-normal sr-only">Previous</b> </a>
                <!-- Right Control -->
                <a class="right carousel-control-next text-danger font-14" href="#slider7" role="button" data-slide="next"> <span class="text-center bg-white ti-arrow-right" aria-hidden="true"></span> <b class="font-normal sr-only">Next</b> </a>
            </div>
            <!-- End of Slider Control -->
        </div>
    </div>
    <!-- End Slider -->
</section> --}}
<!-- ============================================================== -->
<!-- End Slider 1  -->
<!-- ============================================================== -->

{{-- Berita --}}
<div class="blog-home1 spacer ">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <!-- Column -->
            <div class="col-md-8 text-center">
                <h2 class="title font-stylish">Berita PSKP Terbaru</h2>
                <h6 class="subtitle">Berikut daftar berita terbaru Pusat Studi Kesehatan Pariwisata</h6>
            </div>
            <!-- Column -->
            <!-- Column -->
        </div>
        <div class="row m-t-40">

            @foreach ($posts as $post)
                <!-- Column -->
                <div class="col-md-4">
                    <div class="card card-shadow" data-aos="flip-down" data-aos-duration="1200">
                        <a href="#">
                            @if($post->cover_image)
                                <img src="{{ $post->cover_image->getUrl('detail') }}" alt="{{ $post->title }}" class="card-img-top" style="width: 100%">
                            @else
                                <img class="card-img-top" src="/img/default/berita.jpg" alt="gambar" style="width: 100%">
                            @endif
                        </a>
                        <div class="p-30">
                            <div class="d-flex no-block font-14">
                                <a href="#">{{ $post->category->name ?? 'Tidak ada Kategori' }}</a>
                                <span class="ml-auto">{{ Date::parse($post->published_at)->format('d M, Y') }}</span>
                            </div>
                            <h5 class="font-medium m-t-20">
                                <a href="{{ route('postDetail', [$post->id, $post->slug]) }}" class="link">{{ Str::limit($post->title, 45) }}</a>
                            </h5>
                        </div>
                    </div>
                </div>
                <!-- Column -->
            @endforeach

        </div>

        <div class="text-center">
            <a href="{{ route('postIndex', 'berita') }}" class="btn btn-rounded btn-outline-secondary">Lihat Semua Berita</a>
        </div>
    </div>
</div>

{{-- Karya Ilmiah --}}
<div class="spacer feature21 wrap-feature21-box" style="background-image:url(/front/assets/images/features/feature21/img1.jpg)">
    <div class="container">
        <!-- Row  -->
        <div class="row text-white">
            <!-- Column -->
            <div class="col-md-6 both-space">
                <div class="" data-aos="fade-right" data-aos-duration="1200">
                    <h2 class="text-white m-t-20 m-b-30 font-stylish">Karya Ilmiah Remaja</h2>
                    <p class="op-8">Pusat Studi Kesehatan Pariwisata memiliki program Karya Ilmiah Remaja, berikut info dan link pendaftaran.</p>
                    <a href="{{ url('page/18/informasi-karya-ilmiah-remaja.html') }}" class="btn bg-white text-danger btn-rounded btn-md m-t-20"><span> Info</span></a>
                    <a href="#" class="btn btn-danger-gradiant btn-rounded btn-md btn-arrow m-t-20"><span>Pendaftaran <i class="ti-arrow-right"></i></span></a>
                </div>
            </div>
            <!-- Column -->
            @if ($callForPaper)
            <!-- Column -->
            <div class="col-md-6 both-space">
                <div class="" data-aos="fade-right" data-aos-duration="1200">
                    <p class="op-8">Call For Paper</p>
                    <h2 class="text-white m-t-20 m-b-30 font-stylish">{{ $callForPaper->title ?? '-' }}</h2>
                    <a href="{{ route('callForPaper') }}" class="btn btn-danger-gradiant btn-rounded btn-md btn-arrow m-t-20"><span>Registrasi <i class="ti-arrow-right"></i></span></a>
                </div>
            </div>
            <!-- Column -->
            @endif
        </div>
    </div>
</div>
{{-- Download --}}
<div class="spacer feature24 bg-light">
    <div class="container">
        <!-- Row -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <h2 class="title font-stylish">Download PSKP</h2>
                <h6 class="subtitle">Berikut daftar unduhan Pusat Studi Kesehatan Pariwisata</h6>
            </div>
        </div>
        <!-- Row -->
        <div class="row wrap-feature-24">
            <!-- Column -->
            <div class="col-lg-4 col-md-6">
                <div class="card card-shadow" style="border-radius: 0">
                    <a href="{{ url('download/panduan.html') }}" class="service-24"> <i class="icon-Open-Book"></i>
                        <h6 class="ser-title">Panduan</h6>
                    </a>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-4 col-md-6">
                <div class="card card-shadow" style="border-radius: 0">
                    <a href="{{ url('download/ecertificate.html') }}" class="service-24"> <i class="icon-Profile"></i>
                        <h6 class="ser-title">E-certificate</h6>
                    </a>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-4 col-md-6">
                <div class="card card-shadow" style="border-radius: 0">
                    <a href="{{ url('download/form.html') }}" class="service-24"> <i class="icon-Box-withFolders"></i>
                        <h6 class="ser-title">Form</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- Feature 49  -->
<!-- ============================================================== -->
<div class="spacer feature42 bg-danger-gradiant">
    <div class="container">
        <div class="row justify-content-center wrap-feature49-box">
            <div class="col-md-10 col-lg-10 text-center" data-aos="fade-up">
                <h2 class="title text-white font-stylish">Galeri Foto & Video PSKP</h2>
                <h6 class="subtitle text-white op-7 m-b-20">Berikut galeri foto & video Pusat Studi Kesehatan Pariwisata </h6>
                <a href="{{ app(\App\Settings\InformationWebsiteSettings::class)->instagram }}" class="m-b-20"><img src="/front/images/galeri/instagram.png" alt="instagram" /></a>
                <a href="{{ app(\App\Settings\InformationWebsiteSettings::class)->youtube }}" class="m-b-20"><img src="/front/images/galeri/youtube.png" alt="youtube" /></a>
                <a href="{{ route('galleryIndex') }}" class="m-b-20"><img src="/front/images/galeri/virtual.png" alt="virtual" /></a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Feature 49  -->
<!-- ============================================================== -->
@endsection
