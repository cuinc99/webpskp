@extends('frontend.layouts.app')

@section('title')
Download
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Static Slider 10  -->
<!-- ============================================================== -->
<div class="banner-innerpage" style="background-image:url(/assets/images/innerpage/banner-bg2.jpg)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="text-center col-md-8 align-self-center" data-aos="fade-down" data-aos-duration="1200">
                <h1 class="title">Download</h1>
                <h6 class="subtitle op-8">{{ Str::title($cat) }}</h6>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Static Slider 10  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Blog example  -->
<!-- ============================================================== -->
<div class="spacer">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-12">
                <!-- Blog  -->
                <div class="m-t-30 m-b-30">

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Judul</th>
                                    <th>Deskripsi</th>
                                    {{-- <th>Total Unduh</th> --}}
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($downloads as $download)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $download->title ?? '-' }}</td>
                                    <td>{{ $download->description ?? '-' }}</td>
                                    {{-- <td>{{ $download->download_count ?? '0' }}</td> --}}
                                    <td>
                                        @foreach($download->files as $key => $media)
                                            <a href="{{ $media->getUrl() }}" target="_blank" class="btn btn-primary btn-sm btn-rounded" style="margin-bottom: 2px">
                                                Unduh File {{ $key + 1 }}
                                            </a>
                                        @endforeach
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row  -->
    </div>
</div>
<!-- ============================================================== -->
<!-- End Blog example -->
<!-- ============================================================== -->
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
