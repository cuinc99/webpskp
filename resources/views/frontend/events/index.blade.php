@extends('frontend.layouts.app')

@section('title')
    Event PSKP
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Static Slider 10  -->
<!-- ============================================================== -->
<div class="banner-innerpage" style="background-image:url(/assets/images/innerpage/banner-bg2.jpg)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="col-md-8 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
                <h1 class="title">Event PSKP</h1>
                <h6 class="subtitle op-8">Berikut daftar event terbaru dari kegiatan yang akan dilaksanakan oleh PSKP FK Unizar.</h6>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Static Slider 10  -->
<!-- ============================================================== -->
<div class="spacer feature2 bg-light">
    <div class="container">
        <!-- Row  -->
        <div class="row m-t-40">
            <!-- Column -->
            @foreach ($events as $event)
            <div class="col-md-3 wrap-feature2-box">
                <div class="card card-shadow hover1" style="border-radius: 20px">
                    @if($event->banner_image)
                        <a href="{{ $event->banner_image->getUrl() }}" target="_blank" style="display: inline-block">
                            <img class="card-img-top" src="{{ $event->banner_image->getUrl() }}" style="border-radius: 20px;">
                        </a>
                    @else
                        <img class="card-img-top" src="/img/default/agenda.jpg" alt="banner" style="border-radius: 20px;" />
                    @endif
                    <div class="card-body">
                        <h5 class="font-medium text-center">{{ $event->name ?? '' }}</h5>
                        <div class="row">
                            <div class="text-center col-md-6 b-r">
                                <dl class="m-t-20">
                                    <dt>Tanggal Mulai</dt>
                                    <dd>{{ $event->start_at ? Date::parse($event->start_at)->format('l, d M Y') : '' ?? '' }}</dd>
                                </dl>
                            </div>
                            <div class="text-center col-md-6">
                                <dl class="m-t-20">
                                    <dt>Tanggal Selesai</dt>
                                    <dd>{{ $event->start_at ? Date::parse($event->end_at)->format('l, d M Y') : '' ?? '' }}</dd>
                                </dl>
                            </div>
                            <div class="col-md-12 m-t-20" style="text-align: justify;">
                                <span class="font-medium">Keterangan:</span> <br>
                                {!! strip_tags(Str::limit($event->description, 100)) !!}
                            </div>
                            <div class="text-center col-md-12 m-t-20">
                                @if($event->banner_image)
                                <a href="{{ $event->banner_image->getUrl() }}" target="_blank" class="btn btn-danger btn-rounded btn-sm"><i class="fa fa-download"></i></a>
                                @endif
                                <a href="{{ route('eventDetail', [$event->id, $event->slug]) }}" target="_blank" class="btn btn-info btn-rounded btn-sm"> Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="col-md-12">
            <div class="text-center">
                {{ $events->links() }}
            </div>
        </div>
    </div>
</div>


@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
