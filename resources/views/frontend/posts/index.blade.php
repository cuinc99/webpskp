@extends('frontend.layouts.app')

@section('title')
{{ Str::title($cat) }}
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Static Slider 10  -->
<!-- ============================================================== -->
<div class="banner-innerpage" style="background-image:url(/assets/images/innerpage/banner-bg2.jpg)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="text-center col-md-8 align-self-center" data-aos="fade-down" data-aos-duration="1200">
                <h1 class="title">{{ Str::title($cat) }}</h1>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Static Slider 10  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Blog example  -->
<!-- ============================================================== -->
<div class="spacer bg-light blog-home1">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            @foreach ($posts as $post)
                <!-- Column -->
                <div class="col-md-4">
                    <div class="card card-shadow" data-aos="flip-down" data-aos-duration="1200">
                        <a href="#">
                            @if($post->cover_image)
                                <img src="{{ $post->cover_image->getUrl('detail') }}" alt="{{ $post->title }}" class="card-img-top" style="width: 100%">
                            @else
                                <img class="card-img-top" src="/img/default/berita.jpg" alt="gambar" style="width: 100%">
                            @endif
                        </a>
                        <div class="p-30">
                            <div class="d-flex no-block font-14">
                                <span>{{ Date::parse($post->published_at)->format('d M, Y') }}</span>
                            </div>
                            <h5 class="font-medium m-t-20">
                                <a href="{{ route('postDetail', [$post->id, $post->slug]) }}" class="link">{{ Str::limit($post->title, 45) }}</a>
                            </h5>
                        </div>
                    </div>
                </div>
                <!-- Column -->
            @endforeach

            <div class="col-md-12">
                {{ $posts->links() }}
            </div>
        </div>
        <!-- Row  -->
    </div>
</div>
<!-- ============================================================== -->
<!-- End Blog example -->
<!-- ============================================================== -->
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
