@extends('frontend.layouts.app')

@section('title')
{{ $post->category->name ?? '-' }} - {{ $post->title ?? '-' }}
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Static Slider 10  -->
<!-- ============================================================== -->
<div class="banner-innerpage" style="background-image:url(/assets/images/innerpage/banner-bg.jpg)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="text-center col-md-8 align-self-center" data-aos="fade-down" data-aos-duration="1200">
                <h1 class="title">{{ $post->title ?? '-' }}</h1>
                <h6 class="subtitle op-8">{{ Date::parse($post->created_at)->format('d F, Y') ?? '-' }} &#8226; {{ $post->category->name ?? '-' }} &#8226; <a href="#disqus_thread"></a></h6>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Static Slider 10  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Blog example  -->
<!-- ============================================================== -->
<div class="spacer">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-8">
                <!-- Blog  -->
                @if($post->cover_image)
                    <img src="{{ $post->cover_image->getUrl('detail') }}" alt="cover" class="img-fluid" style="width: 100%">
                @else
                    <img class="img-fluid" src="/img/default/berita.jpg" alt="gambar" style="width: 100%">
                @endif
                <div class="m-t-30 m-b-30" style="text-align: justify;">
                    {!! $post->content ?? '-' !!}
                </div>
                <div class="m-t-30">
                    <button type="button" class="btn bg-info btn-rounded"><i class="fa fa-eye"></i> {{ $post->vzt()->count() }} kali dibaca</button>
                </div>
                <hr class="op-5 m-t-30" />
                <!-- comment  -->
                <div class="mini-spacer p-b-0">
                    <div id="disqus_thread"></div>
                    <script>
                        (function() {
                        var d = document, s = d.createElement('script');
                        s.src = 'https://pskp-fk-unizar.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                        })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                </div>
                <!-- Share -->
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-61aebcf98e049c59"></script>

            </div>
            <div class="col-md-4">
                <div class="font-medium d-flex b-b p-b-20 no-block text-uppercase">
                    <h6 class="font-medium no-shrink align-self-center m-b-0">Info Terbaru</h6>
                    <a class="ml-auto text-danger align-self-center" href="{{ route('postIndex', Str::slug(Str::lower($post->category->name))) }}">View All</a>
                </div>
                @foreach ($newPosts as $newPost)
                <div class="row blog-row m-t-30">
                    <div class="col-md-4">
                        @if($newPost->cover_image)
                            <x-smart-image src="{{ $newPost->cover_image->getUrl('detail') }}" class="img-responsive" alt="cover" width="150px" height="100px"/>
                        @else
                            <x-smart-image src="{{ public_path('img/default/berita.jpg') }}" class="img-responsive" alt="cover" width="150px" height="100px"/>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <h5><a href="{{ route('postDetail', [$newPost->id, $post->slug]) }}">{{ $newPost->title }}</a></h5>
                        <p>{{ Date::parse($newPost->published_at)->format('d M, Y') }}</p>
                    </div>
                </div>
                @endforeach

                <div class="font-medium d-flex b-b p-b-20 no-block text-uppercase m-t-40">
                    <h6 class="font-medium no-shrink align-self-center m-b-0">Info Terpopuler</h6>
                    <a class="ml-auto text-danger align-self-center" href="{{ route('postIndex', Str::slug(Str::lower($post->category->name))) }}">View All</a>
                </div>
                @foreach ($popularPosts as $popularPost)
                <div class="row blog-row m-t-30">
                    <div class="col-md-4">
                        @if($popularPost->cover_image)
                            <x-smart-image src="{{ $popularPost->cover_image->getUrl('detail') }}" class="img-responsive" alt="cover" width="150px" height="100px"/>
                        @else
                            <x-smart-image src="{{ public_path('img/default/berita.jpg') }}" class="img-responsive" alt="cover" width="150px" height="100px"/>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <h5><a href="{{ route('postDetail', [$popularPost->id, $post->slug]) }}">{{ $popularPost->title }}</a></h5>
                        <p>Dibaca {{ $popularPost->vzt()->count() }} kali</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <!-- Row  -->
    </div>
</div>
<!-- ============================================================== -->
<!-- End Blog example -->
<!-- ============================================================== -->
@endsection

@section('styles')
<style>
    .attachment__caption {
        display: none;
    }
</style>
@endsection

@section('scripts')
<script id="dsq-count-scr" src="//website-fk-unizar.disqus.com/count.js" async></script>
<script type="text/javascript">
    $("document").ready(function() {
        $("img").addClass("img-responsive");
    });
</script>
@endsection
