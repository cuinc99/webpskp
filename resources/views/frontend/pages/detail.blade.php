@extends('frontend.layouts.app')

@section('title')
{{ $page->title ?? '-' }}
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Static Slider 10  -->
<!-- ============================================================== -->
<div class="banner-innerpage" style="background-image:url(/assets/images/innerpage/banner-bg2.jpg)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="text-center col-md-8 align-self-center" data-aos="fade-down" data-aos-duration="1200">
                <h1 class="title">{{ $page->title ?? '-' }}</h1>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Static Slider 10  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Blog example  -->
<!-- ============================================================== -->
<div class="spacer">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-8">
                <!-- Blog  -->
                <div class="m-t-30 m-b-30" style="text-align: justify;">
                    {!! $page->content ?? '-' !!}
                </div>
            </div>
        </div>
        <!-- Row  -->
    </div>
</div>
<!-- ============================================================== -->
<!-- End Blog example -->
<!-- ============================================================== -->
@endsection

@section('styles')
<style>
    .attachment__caption {
        display: none;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript">
    $("document").ready(function() {
        $("img").addClass("img-responsive");
    });
</script>
@endsection
