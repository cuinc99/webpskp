@extends('frontend.layouts.app')

@section('title')
{{ $event->name ?? '-' }}
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Static Slider 10  -->
<!-- ============================================================== -->
<div class="banner-innerpage" style="background-image:url(/assets/images/innerpage/banner-bg2.jpg)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="text-center col-md-8 align-self-center" data-aos="fade-down" data-aos-duration="1200">
                <h1 class="title">{{ $event->name ?? '-' }}</h1>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Static Slider 10  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Blog example  -->
<!-- ============================================================== -->
<div class="spacer">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-8 wrap-feature2-box">
                <div class="card card-shadow hover1" style="border-radius: 20px">
                    @if($event->banner_image)
                        <a href="{{ $event->banner_image->getUrl() }}" target="_blank" style="display: inline-block">
                            <img class="card-img-top" src="{{ $event->banner_image->getUrl() }}" style="border-radius: 20px;">
                        </a>
                    @else
                        <img class="card-img-top" src="/img/default/agenda.jpg" alt="banner" style="border-radius: 20px;" />
                    @endif
                    <div class="card-body">
                        <h3 class="font-medium text-center">{{ $event->name ?? '' }}</h3>
                        <div class="row">
                            <div class="text-center col-md-6 b-r">
                                <dl class="m-t-20">
                                    <dt>Tanggal Mulai</dt>
                                    <dd>{{ $event->start_at ? Date::parse($event->start_at)->format('l, d M Y') : '' ?? '' }}</dd>
                                </dl>
                            </div>
                            <div class="text-center col-md-6">
                                <dl class="m-t-20">
                                    <dt>Tanggal Selesai</dt>
                                    <dd>{{ $event->start_at ? Date::parse($event->end_at)->format('l, d M Y') : '' ?? '' }}</dd>
                                </dl>
                            </div>
                            <div class="col-md-12 m-t-20" style="text-align: justify;">
                                <span class="font-medium">Keterangan:</span> <br>
                                {!! $event->description ?? '' !!}
                            </div>
                            <div class="text-center col-md-12 m-t-20">
                                @if($event->banner_image)
                                <a href="{{ $event->banner_image->getUrl() }}" target="_blank" class="btn btn-danger btn-rounded"><i class="fa fa-download"></i> Unduh Flayer</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row  -->
    </div>
</div>
<!-- ============================================================== -->
<!-- End Blog example -->
<!-- ============================================================== -->
@endsection

@section('styles')
<style>
    .attachment__caption {
        display: none;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript">
    $("document").ready(function() {
        $("img").addClass("img-responsive");
    });
</script>
@endsection
