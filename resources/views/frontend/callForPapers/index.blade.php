@extends('frontend.layouts.app')

@section('title')
    Call For Paper | {{ $callForPaper->title }}
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Static Slider 10  -->
<!-- ============================================================== -->
<div class="banner-innerpage" style="background-image:url(/assets/images/innerpage/banner-bg2.jpg)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="col-md-8 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
                <h6 class="subtitle op-8">Call For Paper</h6>
                <h1 class="title">{{ $callForPaper->title }}</h1>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Static Slider 10  -->
<!-- ============================================================== -->
<div class="spacer feature2 bg-light">
    <div class="container">
        <!-- Row  -->
        <div class="row m-t-40">
            @if (session()->has('message'))
            <div class="col-12">
                <div class="alert alert-success alert-rounded"> <i class="ti-check"></i> {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            </div>
            @endif
            <div class="col-lg-8">
                <div class="contact-box p-r-40">
                    <form method="POST" action="{{ route('callForPaperPost') }}" data-aos="fade-left" data-aos-duration="1200" class="aos-init aos-animate">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group m-t-15">
                                    <label>Nama Lengkap</label>
                                    <input class="form-control" type="text" name="name" placeholder="Nama Lengkap dengan Gelar" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-t-15">
                                    <label>Email</label>
                                    <input class="form-control" type="email" name="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-t-15">
                                    <label>Nomor HP</label>
                                    <input class="form-control" type="number" name="phone_number" placeholder="Nomor HP" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-t-15">
                                    <label>Jurusan/Program Studi</label>
                                    <input class="form-control" type="text" name="major" placeholder="Jurusan/Program Studi" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-t-15">
                                    <label>Status</label>
                                    <select name="status" class="form-control" required>
                                        <option value="Mahasiswa">Mahasiswa</option>
                                        <option value="Dosen">Dosen</option>
                                        <option value="Tenaga Kesehatan">Tenaga Kesehatan</option>
                                        <option value="Lain-Lain">Lain-Lain</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-t-15">
                                    <label>Instansi</label>
                                    <input class="form-control" type="text" name="agency" placeholder="Instansi" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-t-15">
                                    <label>Jumlah Artikel</label>
                                    <input class="form-control" type="number" name="number_of_article" placeholder="Jumlah Artikel" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-t-15">
                                    <label>Tipe Presentasi</label>
                                    <select name="type_presentation" class="form-control" required>
                                        <option value="Oral Presentation">Oral Presentation</option>
                                        <option value="Poster Presentation">Poster Presentation</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group m-t-15">
                                    <label>Judul Sementara</label>
                                    <input class="form-control" type="text" name="temporary_title" placeholder="Judul Sementara" required>
                                </div>
                            </div>
                            <input type="hidden" name="call_for_paper_id" value="{{ $callForPaper->id }}">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-danger-gradiant m-t-20 btn-arrow"><span> SUBMIT <i class="ti-arrow-right"></i></span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="detail-box p-40 bg-info aos-init aos-animate" data-aos="fade-right" data-aos-duration="1200">
                    <h2 class="text-white">Deskripsi</h2>
                    <span class="text-white m-t-30 op-8" style="color: white !important;">
                        {!! $callForPaper->description ?? '-' !!}
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
