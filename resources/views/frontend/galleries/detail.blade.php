@extends('frontend.layouts.app')

@section('title')
    {{ $gallery->title }}
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Static Slider 10  -->
<!-- ============================================================== -->
<div class="banner-innerpage" style="background-image:url(/assets/images/innerpage/banner-bg2.jpg)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="text-center col-md-8 align-self-center" data-aos="fade-down" data-aos-duration="1200">
                <h1 class="title">{{ $gallery->title ?? '-' }}</h1>
                <h6 class="subtitle op-8">Galeri {{ $gallery->type ?? '-' }}</h6>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Static Slider 10  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Portfolio 5 -->
<!-- ============================================================== -->
<div class="spacer feature2" id="work">
    <div class="container">
        <div class="row m-t-40 popup-gallery">

            @if ($gallery->type === 'Photo')
                @forelse($gallery->photo as $key => $media)
                    <div class="col-md-4">
                        <div class="card">
                            <a href="{{ $media->getUrl() }}" class="img-ho" title="{{ $gallery->title }} {{ $key + 1 }}">
                                <img class="card-img-top" src="{{ $media->getUrl() }}" alt="{{ $gallery->title }}" />
                            </a>
                        </div>
                    </div>
                @empty
                    <div class="col-md-12">
                        <div class="alert alert-danger" role="alert">
                            Maaf, Belum ada foto yang ditambahkan pada galeri ini.
                          </div>
                    </div>
                @endforelse
            @else
                <div class="col-md-12">
                    <x-embed url="{{ $gallery->link }}" />
                </div>
            @endif

            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-61aebcf98e049c59"></script>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Projects -->
<!-- ============================================================== -->
@endsection

@section('styles')
    <link href="/front/assets/node_modules/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
    <link href="/front/css/portfolio/portfolio.css" rel="stylesheet">

    @if ($gallery->type === 'Video')
        <x-embed-styles />
    @endif
@endsection

@section('scripts')
    <script src="/front/assets/node_modules/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script src="/assets/js/portfolio.js"></script>
@endsection
