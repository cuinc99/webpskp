@extends('frontend.layouts.app')

@section('title')
Galery Foto & Video
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Static Slider 10  -->
<!-- ============================================================== -->
<div class="banner-innerpage" style="background-image:url(/assets/images/innerpage/banner-bg2.jpg)">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="text-center col-md-8 align-self-center" data-aos="fade-down" data-aos-duration="1200">
                <h1 class="title">Galery Foto & Video</h1>
                <h6 class="subtitle op-8">Berikut daftar galeri foto dan video dari PSKP FK Unizar.</h6>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Static Slider 10  -->
<!-- ============================================================== -->
<div class="spacer feature2 bg-light">
    <div class="container">
        <!-- Row  -->
        <div class="row m-t-40">
            @foreach ($galleries as $gallery)
            <div class="col-md-4">
                <div class="card card-shadow">
                    <a href="{{ route('galleryDetail', [$gallery->id, $gallery->slug]) }}" class="img-ho">
                        @if($gallery->cover_image)
                            <img class="card-img-top" src="{{ $gallery->cover_image->getUrl('detail') }}">
                        @else
                            <img class="card-img-top" src="/assets/images/case-study/img1.jpg" alt="cover" />
                        @endif
                    </a>
                    <div class="card-body">
                        <h5 class="font-medium m-b-0">{{ Str::limit($gallery->title, 25, '...') }}</h5>
                        <p class="m-b-0 font-14">Galeri {{ $gallery->type }}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="text-center">
            {{ $galleries->links() }}
        </div>
    </div>
</div>

@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
