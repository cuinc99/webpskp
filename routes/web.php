<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])->name('index');

Route::get('/post/{category}.html', [FrontendController::class, 'postIndex'])->name('postIndex');
Route::get('/post/{post}/{slug}.html', [FrontendController::class, 'postDetail'])->name('postDetail');

Route::get('/page/{page}/{slug}.html', [FrontendController::class, 'pageDetail'])->name('pageDetail');

Route::get('/events.html', [FrontendController::class, 'eventIndex'])->name('eventIndex');
Route::get('/event/{event}/{slug}.html', [FrontendController::class, 'eventDetail'])->name('eventDetail');

Route::get('/galleries.html', [FrontendController::class, 'galleryIndex'])->name('galleryIndex');
Route::get('/gallery/{gallery}/{slug}.html', [FrontendController::class, 'galleryDetail'])->name('galleryDetail');

Route::get('/download/{category}.html', [FrontendController::class, 'downloadIndex'])->name('downloadIndex');

Route::get('/call-for-paper.html', [FrontendController::class, 'callForPaper'])->name('callForPaper');
Route::post('/call-for-paper-store', [FrontendController::class, 'callForPaperPost'])->name('callForPaperPost');
